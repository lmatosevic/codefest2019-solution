from scipy.spatial import distance as dist
from imutils import contours
import numpy as np
import imutils
import cv2
import os
from random import randint, shuffle
import time


# cable detection code


def midpoint(point_a, point_b):
    return (point_a[0] + point_b[0]) * 0.5, (point_a[1] + point_b[1]) * 0.5


def image_has_cable(image, min_width, min_length, max_width, max_length, ratio=0):
    """Returns an information weather an image contains a cable

    Parameters:
        image: cv2 image
        min_width(int): minimal cable width
        min_length: minimal cable length
        max_width: maximal cable width
        max_length: maximal cable length

    Returns:
        has_cable(boolean): true if image contains a cable, otherwise false
    """

    # convert image to grayscale to ease edge detection
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # blur image to get rid of noise
    blured_image = cv2.GaussianBlur(gray_image, (5, 5), 0)

    # perform canny filter for edge detection (cable's edges)
    edged_image = cv2.Canny(blured_image, 85, 85)

    # dilation followed by erosion closes gaps in objects (noise inside cables)
    edged_image = cv2.dilate(edged_image, None, iterations=1)
    edged_image = cv2.erode(edged_image, None, iterations=1)

    # find contours in the edge map
    contours_list = cv2.findContours(edged_image.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours_list = contours_list[1] if imutils.is_cv3() else contours_list[0]
    (contours_list, _) = contours.sort_contours(contours_list)

    has_cable = False

    for contour in contours_list:
        box = cv2.minAreaRect(contour)
        box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box)
        box = np.array(box, dtype="int")

        (point_top_left, point_top_right, point_bottom_right, point_bottom_left) = box
        (top_midpoint_x, top_midpoint_y) = midpoint(point_top_left, point_top_right)
        (bottom_midpoint_x, bottom_midpoint_y) = midpoint(point_bottom_left, point_bottom_right)
        (left_midpoint_x, left_midpoint_y) = midpoint(point_top_left, point_bottom_left)
        (right_midpoint_x, right_midpoint_y) = midpoint(point_top_right, point_bottom_right)

        width, length = dist.euclidean((top_midpoint_x, top_midpoint_y),
                                       (bottom_midpoint_x, bottom_midpoint_y)), dist.euclidean(
            (left_midpoint_x, left_midpoint_y), (right_midpoint_x, right_midpoint_y))

        if width < min_width or width > max_width or length < min_length or length > max_length or length < 0 * width:
            continue
        else:
            has_cable = True

    return has_cable


# genetic algorithm code


def goodness(dataset, min_width, min_length, max_width, max_length):
    """Calculates goodness(percentage of rightly predicted) of given parameters on a dataset for cables detection.

            Parameters:
                dataset(str): routers dataset (black or white)
                min_width(int): minimal cable width
                min_length: minimal cable length
                max_width: maximal cable width
                max_length: maximal cable length


            Returns:
                (goodness_all, goodness_on, goodness_off): a tuple containing percentages of rightly predicted images
    """
    wrong_on, right_on = 0, 0
    wrong_off, right_off = 0, 0

    image_path_black_on = "training_set\\" + dataset + "\\kablovi\\ustekano\\"
    files = os.listdir(image_path_black_on)
    shuffle(files)

    # images = [cv2.imread(image_path_black_on + file) for file in files][0:50]
    images = [cv2.imread(image_path_black_on + file) for file in files]

    for image in images:
        res = image_has_cable(image, min_width, min_length, max_width, max_length)
        if res:
            right_on += 1
        else:
            wrong_on += 1
    goodness_on = right_on / (right_on + wrong_on)

    image_path_black_on = "training_set\\" + dataset + "\\kablovi\\istekano\\"
    files = os.listdir(image_path_black_on)
    shuffle(files)

    # images = [cv2.imread(image_path_black_on + file) for file in files][0:50]
    images = [cv2.imread(image_path_black_on + file) for file in files]

    for image in images:
        res = image_has_cable(image, min_width, min_length, max_width, max_length)
        if res:
            wrong_off += 1
        else:
            right_off += 1

    goodness_off = right_off / (right_off + wrong_off)

    goodness_all = (goodness_on + goodness_off) / 2

    return goodness_all, goodness_on, goodness_off


def crossover_pop(population_1, population_2):
    """Get a crossover population from two parent populations on averaging their values with upper and lower limits.

                Parameters:
                    population_1: contains dimension values  in a tuple representing a population
                    population_2: contains dimension values  in a tuple representing a population

                Returns:
                    new_population: population generated from two parent populations
    """
    new_pop = (mean_with_limit(population_1[0], population_2[0], 0, 100),
               mean_with_limit(population_1[1], population_2[1], 0, 200),
               mean_with_limit(population_1[2], population_2[2], 100, 400),
               mean_with_limit(population_1[2], population_2[2], 200, 400))

    return new_pop


def mean_with_limit(el1, el2, low_lim, up_lim):
    el = int((el1 + el2) / 2)
    el = el if el <= up_lim else up_lim
    el = el if el >= low_lim else low_lim
    return el


def optimize_parameters_with_genetic_algorithm(dataset, popsize=20, max_iterations=1000):
    """Optimize the image_has_led function using elitistic generational genetic algorithm.

                Parameters:
                    dataset(str): black or white
                    popsize: population size
                    max_iterations: max iterations
    """

    # generate initial population using some empirically found good and some random values
    population = [(50, 103, 334, 374)]
    for i in range(0, popsize + 1):
        population.append((randint(0, 100), randint(0, 200), randint(100, 400), randint(200, 400)))

    maxv = 0
    best_pop = []

    for i in range(0, max_iterations):
        start = time.time()
        print("#####")
        print("iteration: " + str(i + 1))
        fitness = []

        # sort by goodness
        for i, pop in enumerate(population):
            start_pop = time.time()
            # print(i, population)
            goodness_value = goodness(dataset, population[i][0], population[i][1], population[i][2], population[i][3])[
                0]
            fitness.append((goodness_value, i))

            best_pop = population[i] if goodness_value > maxv else best_pop
            maxv = goodness_value if goodness_value > maxv else maxv

            print("pop: " + str(i) + "/" + str(popsize) + " - " + str(population[i]) + " in " + str(
                time.time() - start_pop) + " seconds with goodness " + str(goodness_value))

        fitness.sort(key=lambda a: a[0], reverse=True)
        new_population = []
        for fit in fitness[:5]:
            new_population.append(population[fit[1]])

        # elitism
        population = new_population.copy()

        # population = []
        while len(population) < popsize:
            population.append(crossover_pop(population[randint(0, 4)], population[randint(0, 4)]))

        # mutation towards good heuristic
        for i in range(popsize - 5):
            if randint(0, 5) == 1:
                population[i + 4] = crossover_pop(population[i + 4],
                                                  (randint(0, 100), randint(0, 200), randint(100, 400),
                                                   randint(200, 400)))

        print("max: " + str(maxv))
        print("best pop: " + str(best_pop))
        print("took: " + str(time.time() - start) + "seconds.")


def main():
    # black (ratio = 5.75)
    # population = [((30, 100, 100), (50, 255, 255), 10)]
    # population = [((75, 15, 40), (138, 132, 47), 4)]
    # white
    # population = [((23, 75, 88), (100, 255, 255), 9)]
    # population = [((140, 98, 61), (168, 46, 95), 9)]
    # population = [((19, 60, 151), (56, 218, 193), 25)]
    # population = [(6, 62, 164, 359)]
    # population = [(5, 46, 205, 345)]

    # population = [(18, 64, 187, 350)]
    # population = [(29, 11, 190, 297)]
    # population = [(30, 80, 150, 320)]
    # population = [(6, 62, 164, 359)]
    # population = [(38, 45, 159, 329), (53, 119, 158, 396)]
    # population = [(50, 103, 334, 374)]

    # print(population[0])
    # print(goodnes(SET, population[0][0], population[0][1], population[0][2], population[0][3]))
    # exit()

    optimize_parameters_with_genetic_algorithm('white')


if __name__ == '__main__':
    main()
