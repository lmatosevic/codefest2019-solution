import cv2
import flask
import numpy as np
from PIL import Image
from flask import request
from keras.models import load_model

# initialize our Flask application and the Keras model
from src.cabel.detect_cable_status import image_has_cable
from src.led.detect_led_status import image_has_led

app = flask.Flask(__name__)
model = None

IMG_DIR = '/opt/lampp/htdocs/tech-support-solution/storage/app/images/'


def main():
    global model
    model = load_model('../model/model.10-2.03.h5')
    model._make_predict_function()
    app.run()


@app.route("/predict", methods=["GET", "POST"])
def predict():
    global model
    img_size = 699

    # Loading and preparing image
    image_data = []
    name = request.args.get('name')
    img = Image.open(IMG_DIR + str(name))
    img = img.convert('L')
    img = img.resize((img_size, img_size), Image.ANTIALIAS)
    img = np.array(img)
    image_data.append([img, 'test'])

    images = np.array([i[0] for i in image_data]).reshape(-1, img_size, img_size, 1)
    preds = model.predict(np.expand_dims(images[0], axis=0))
    values = preds.tolist()
    if values[0][0] == 1:
        predicted = 1  # Black
    else:
        predicted = 0  # White
    with open("../out/results.txt", "a") as file:
        file.write('Img: ' + str(name) + ' - ' + str(predicted) + '\n')
    return str(predicted)


@app.route("/led", methods=["GET", "POST"])
def detect_led():
    name = request.args.get('name')
    color = request.args.get('color')

    if color == '1':  # black
        predicted = image_has_led(cv2.imread(IMG_DIR + str(name)), 10, (30, 100, 100), (50, 255, 255))
    else:  # white
        predicted = image_has_led(cv2.imread(IMG_DIR + str(name)), 9, (23, 75, 88), (100, 255, 255))
    with open("../out/results.txt", "a") as file:
        file.write('Img LED (' + str(color) + '): ' + str(name) + ' - ' + str(predicted) + '\n')
    return str(predicted)


@app.route("/cabel", methods=["GET", "POST"])
def detect_cabel():
    name = request.args.get('name')
    color = request.args.get('color')

    if color == '1':  # black
        predicted = image_has_cable(cv2.imread(IMG_DIR + str(name)), 6, 62, 164, 359)
    else:  # white
        predicted = image_has_cable(cv2.imread(IMG_DIR + str(name)), 21, 141, 116, 343)

    if predicted:
        predicted = 1
    else:
        predicted = 0

    with open("../out/results.txt", "a") as file:
        file.write('Img CABLE (' + str(color) + '): ' + str(name) + ' - ' + str(predicted) + '\n')
    return str(predicted)


if __name__ == "__main__":
    main()
