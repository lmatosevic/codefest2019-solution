## Router prediction HTTP service
This service provides 3 HTTP endpoints for detecting router type, LEDs and cabels statuses. It si developed using simple
Flask framework for exposing those 3 GET endpoints at following urls:
1. /predict - returns 1 or 0 depending if the router is Ubee or Huawei type
2. /led - returns 1 if LEDs are On, or 0 if they are turned off
3. /cabel - returns 1 if cabels are detected in image or 0 if not

All endpoints accept image name as parameter "name". Also led and cabel accepts also the color of the router as parameter
"color" which can be 0 or 1. Image name is the name of iamge locted in backend storage folder depending of the web
server filesystem location (e.g. /opt/lampp/htdocs/tech-support-solution/storage/app/images/) and is configured as global
varaible in main.py file.

## Execution
Run the flask server by executing following command: "python main.py"
\* All required libraries: tensorflow, keras, openCv should be installed on the system before running the service.