<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use Laravel\Lumen\Routing\Router;
use Lujo\Lumen\Rest\RestRoute;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/**
 * @var $router Router
 */


$router->get('/', function () use ($router) {
    return $router->app->version();
});

// REST routes
$router->group(
    ['prefix' => '', 'namespace' => 'Rest'],
    function (Router $restRouter) {
        RestRoute::route($restRouter, 'users', 'UserController');
        RestRoute::route($restRouter, 'messages', 'MessageController');
        RestRoute::route($restRouter, 'tasks', 'TaskController');
        RestRoute::route($restRouter, 'equiment', 'EquipmentController');
    }
);

$router->group(
    ['prefix' => 'bot'],
    function (Router $msgRoute) {
        $msgRoute->get('listen', 'BotController@listen');
        $msgRoute->post('listen', 'BotController@listen');
    }
);


$router->get('/clear', function() {
    Cache::forget('2291131174301220');
    return "Cache is cleared";
});

$router->get('/state', function() {
    return Cache::get('2291131174301220');
});


$router->get('images/{filename}', 'AssetController@image');

