## Technical support ChatBot (Facebook)
This solution is backend implementation of Facebook chatbot using Mealy State Machine to provide router problem 
detection and resolving solution in an interactive way. Facebook Messenger serves only as a interface for 
sending/receiving messages with customer, all the logic and state management is handled in this Lumen/PHP backend 
service. Service for router classification, led and cabel detection is in separate Python (ROUTER_PREDICTION_SERVICE) 
service which is called on demand with received image from user to perform real-time analysis on.

## Installation
1. Duplicate ".env.example" file in root directory and rename it to ".env" 
2. Change ".env" configurations for  DB_\*, ROUTER_PREDICTION_SERVICE and FACEBOOK_\* connection configuration.
3. Run in the command line: php composer.phar install
4. Run: php artisan key:generate
5. Run: php artisan passport:install
6. Run: php artisan db:migrate
7. Run: php artisan db:seed
8. Deploy all the files to web server root folder (apache, ngnix, IIS10 or any other PHP web server)
9. Navigate with browser to http://localhost/technical-support-solution/public/v1/ it should print framework version

## Facebook integration
Create facbook page, application and configure messenger api webhooks.
Official tutorial used: https://developers.facebook.com/docs/messenger-platform/getting-started/quick-start
When page/app is created, update the .env file FACEBOOK_\* configurations based on APP secret and API token codes.

\* Since Facebook webhooks require secure HTTPS protocol access, use tools like https://ngrok.com/ to expose the 
localhost environment so facebook can forward messages and images received from customers from messenger application.

## Solution code
ChatBot endpoint is implemented in _App/Http/Controllers/BotController.php_ class. It uses _App/Service/BotService.php_ 
and _StateService.php_ classes for managing states and persisting tasks in the database.