<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Response;

class AdminGuardMiddleware extends AuthenticateMiddleware {

    /**
     * Create a new middleware instance.
     *
     * @param Auth|\Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Auth $auth) {
        parent::__construct($auth);
    }

    /**
     * Restrict access to admin only.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param mixed $guard
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        $error = $this->checkAuthorization($guard);
        if ($error !== null) {
            return $error;
        }
        if ($request->user()->can('admin')) {
            return $next($request);
        }
        return response()->json([
            'success' => false,
            'status' => Response::HTTP_FORBIDDEN,
            'message' => 'HTTP_FORBIDDEN'
        ], Response::HTTP_FORBIDDEN);
    }
}