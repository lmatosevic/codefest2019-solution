<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Http\Response;

class AuthenticateMiddleware {
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory $auth
     */
    public function __construct(Auth $auth) {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        $error = $this->checkAuthorization($guard);

        if (!$this->isOauthAccessValid($request)) {
            return response()->json([
                'success' => false,
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'HTTP_UNAUTHORIZED'
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($error !== null) {
            return $error;
        }

        return $next($request);
    }

    protected function checkAuthorization($guard) {
        if ($this->auth->guard($guard)->guest()) {
            return response()->json([
                'success' => false,
                'status' => Response::HTTP_UNAUTHORIZED,
                'message' => 'HTTP_UNAUTHORIZED'
            ], Response::HTTP_UNAUTHORIZED);
        }
        return null;
    }

    private function isOauthAccessValid($request) {
        if ($request->user() === null || $request->user()->can('admin')) {
            return true;
        }
        if ($request->is('v1/oauth/token') || $request->is('v1/oauth/token/refresh')) {
            if ($request->isMethod('POST')) {
                return true;
            } else {
                return false;
            }
        } else if ($request->is('v1/oauth/*')) {
            return false;
        } else {
            return true;
        }
    }
}
