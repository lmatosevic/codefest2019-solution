<?php

namespace App\Http\Middleware;

use Closure;

class CorsMiddleware {
    /**
     * Handle an incoming request and add required headers for cross origin access.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $headers = [
            'Access-Control-Allow-Origin' => config('util.cors.origin'),
            'Access-Control-Allow-Methods' => config('util.cors.methods'),
            'Access-Control-Allow-Credentials' => config('util.cors.credentials'),
            'Access-Control-Max-Age' => config('util.cors.maxage'),
            'Access-Control-Allow-Headers' => config('util.cors.headers'),
            "Access-Control-Expose-Headers" => config('util.cors.expose'),
        ];

        if ($request->isMethod('OPTIONS')) {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        foreach ($headers as $key => $value) {
            $response->headers->set($key, $value);
        }

        return $response;
    }
}