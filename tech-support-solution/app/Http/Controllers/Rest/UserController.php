<?php

namespace App\Http\Controllers\Rest;


use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Lujo\Lumen\Rest\RestController;

class UserController extends RestController {

    /**
     * Return the specific model object of a resource for child controller.
     *
     * @return Model
     */
    protected function getModel() {
        return new User();
    }
}