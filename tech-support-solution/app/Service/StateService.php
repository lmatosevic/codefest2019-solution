<?php


namespace App\Service;


class StateService {

    public const STATES = [
        '0' => [['action' => '', 'next' => '1']],
        '1' => [['action' => 'knows', 'next' => '5'], ['action' => 'dont_know', 'next' => '2']],
        '2' => [['action' => 'success', 'next' => '5'], ['action' => 'fail_1', 'next' => '4'],  // accepts image
            ['action' => 'fail_2', 'next' => '3']],
        '3' => [['action' => 'choosen', 'next' => '5']],
        '4' => [['action' => 'send_img', 'next' => '2']],
        '5' => [['action' => 'success', 'next' => '6'], ['action' => 'stay', 'next' => '5']], // accepts image
        '6' => [['action' => 'success', 'next' => '7'], ['action' => 'stay', 'next' => '6'],
            ['action' => 'fail', 'next' => '5']], // accepts image
        '7' => [['action' => 'tell_problem', 'next' => '8']],
        '8' => [['action' => 'fixed', 'next' => '0'], ['action' => 'not_fixed', 'next' => '5']],
        '9' => [['action' => '', 'next' => '0']]
    ];

    public static function getNextState($currentState, $action) {
        try {
            $states = self::STATES[$currentState];
            foreach ($states as $state) {
                if ($state['action'] === $action) {
                    return $state['next'];
                }
            }
        } catch (\Exception $e) {
            return '0';
        }
        return $currentState;
    }
}