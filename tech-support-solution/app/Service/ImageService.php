<?php

namespace App\Services;

use Exception;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class ImageService {
    public const ALLOWED_TYPES = ['jpg', 'jpeg', 'gif', 'png'];
    private const FOLDER_PATH = 'images/';
    private const THUMBNAILS_PATH = 'images/thumbnails/';

    public static function base64ToImage($data, $id) {
        if (!$data) {
            return '';
        }
        if (preg_match('/^data:image\\/(\\w+);base64,/', $data, $type)) {
            $data = substr($data, strpos($data, ',') + 1);
            $type = strtolower($type[1]);

            if (!in_array($type, self::ALLOWED_TYPES)) {
                throw new Exception('Invalid image type ' . $type);
            }

            $data = base64_decode($data);

            if ($data === false) {
                throw new Exception('Base64 decoding failed');
            }
        } else {
            throw new Exception('Did not match data URI with image data');
        }

        $fileName = self::generateName($id, $type);
        if (!Storage::disk('local')->put(self::FOLDER_PATH . $fileName, $data)) {
            throw new Exception('Unable to save image on filesystem');
        }
        return $fileName;
    }

    public static function fetchImage($url) {
        $fileName = self::generateName(1, 'jpg');
        $data = file_get_contents($url);
        if (!Storage::disk('local')->put(self::FOLDER_PATH . $fileName, $data)) {
            throw new Exception('Unable to save image on filesystem');
        }
        return $fileName;
    }

    public static function createImageThumbnail($imageName, $id, $width = 300, $height = null) {
        $img = Image::make(Storage::disk('local')->get(self::FOLDER_PATH . $imageName));
        if (($width && $img->width() > $width) || ($height && $img->height() > $height)) {
            $img->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            });
        }
        $type = pathinfo($imageName, PATHINFO_EXTENSION);
        $thumbName = 'tmb-' . self::generateName($id, $type);
        if (!Storage::disk('local')->put(self::THUMBNAILS_PATH . $thumbName, (string)$img->encode($type))) {
            throw new Exception('Unable to save thumbnail on filesystem');
        }
        return $thumbName;
    }

    public static function imageToBase64($imageName) {
        $content = Storage::disk('local')->get(self::FOLDER_PATH . $imageName);
        return base64_encode($content);
    }

    public static function rawImage($imageName) {
        return Storage::disk('local')->get(self::FOLDER_PATH . $imageName);
    }

    public static function rawThumbnail($imageName) {
        return Storage::disk('local')->get(self::THUMBNAILS_PATH . $imageName);
    }

    public static function deleteImage($imageName) {
        return Storage::disk('local')->delete(self::FOLDER_PATH . $imageName);
    }

    public static function deleteThumbnail($imageName) {
        return Storage::disk('local')->delete(self::THUMBNAILS_PATH . $imageName);
    }

    public static function generateName($id, $type) {
        return str_replace([' ', '.'], '', microtime()) . '-' . hash('md5', $id) . '.' . $type;
    }
}