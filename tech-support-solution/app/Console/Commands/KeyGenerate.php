<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class KeyGenerate extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "key:generate";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Generate app key";

    /**
     * Generate new app key with length of 32 bits.
     *
     * @return mixed
     */
    public function handle() {
        $key = str_random(32);
        $this->updateKey('.env', '/(APP_KEY=)(.*)/', '${1}' . $key);
        $this->updateKey('config/app.php', '/(\'key\'\s*=>\s*env\(\'APP_KEY\',\s*)\'(.*)\'\),/', '${1}\'' . $key. '\'),');
        echo "App key generated and updated ({$key})" . PHP_EOL;
        return 0;
    }

    private function updateKey($fileName, $pattern, $replacement) {
        $content = file_get_contents($fileName);
        $envUpdated = preg_replace($pattern, $replacement, $content);
        file_put_contents($fileName, $envUpdated);
        echo "Updated " . $fileName . PHP_EOL;
    }
}