<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler {
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $e
     * @return void
     */
    public function report(Exception $e) {
        if (!config('app.debug')) {
            if ($this->shouldReport($e)) {
                $this->logError($e);
            }
        } else {
            parent::report($e);
        }
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e) {
        if ($request->wantsJson()) {
            $success = false;
            $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            if ($e instanceof HttpResponseException) {
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
            } else if ($e instanceof MethodNotAllowedHttpException) {
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                $e = new MethodNotAllowedHttpException([], 'HTTP_METHOD_NOT_ALLOWED', $e);
            } else if ($e instanceof NotFoundHttpException) {
                $status = Response::HTTP_NOT_FOUND;
                $e = new NotFoundHttpException('HTTP_NOT_FOUND', $e);
            } else if ($e instanceof AuthorizationException) {
                $status = Response::HTTP_FORBIDDEN;
                $e = new AuthorizationException('HTTP_FORBIDDEN', $status);
            } else if ($e instanceof \Dotenv\Exception\ValidationException && $e->getResponse()) {
                $status = Response::HTTP_BAD_REQUEST;
                $e = new \Dotenv\Exception\ValidationException('HTTP_BAD_REQUEST', $status, $e);
            }

            $message = $e->getMessage();
            $finalStatus = $message !== "" ? $status : Response::HTTP_NOT_FOUND;

            return response()->json([
                'success' => $success,
                'status' => $finalStatus,
                'message' => $message
            ], $finalStatus);
        }

        return parent::render($request, $e);
    }

    private function logError($e) {
        $line = array_key_exists('line', $e->getTrace()[0]) ? $e->getTrace()[0]['line'] : 'unknown';
        $file = array_key_exists('file', $e->getTrace()[0]) ? $e->getTrace()[0]['file'] : 'unknown';
        Log::error('[' . $e->getCode() . '] "' . $e->getMessage() . '" on line ' . $line . ' of file ' . $file);
    }
}
