<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Admin user with password: Password1!
        DB::table('user')->insert([
            ['email' => 'info@green-light.agency', 'username' => 'admin', 'first_name' => 'Admin',
                'last_name' => 'Admin', 'phone_number' => '385981707958',
                'password_hash' => '$2y$10$vcz2GeU9DuBYX3REM9tESuuZh76cKxI38rK0vQPNY5TZgflCvVPhy',
                'account_type' => 'ADMIN', 'image_file' => null, 'enabled' => true,
                'created_at' => '2019-05-21 01:01:01', 'updated_at' => '2019-05-21 01:01:01']
        ]);
    }
}