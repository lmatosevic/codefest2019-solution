import os
from random import shuffle

import numpy as np
from PIL import Image
from keras.utils import to_categorical
from sklearn.preprocessing import LabelEncoder
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.layers.normalization import BatchNormalization
from keras.models import Sequential

from squeezenet import SqueezeNet

DIR = 'resources/'
TRAIN_DIR = DIR + 'train'
TEST_DIR = DIR + 'test'
OUT = 'out/'


def main():
    labels = ['black', 'white']
    print('Calculating image size statistics...')
    img_size = get_size_statistics(labels)
    print('Average image size: ' + str(img_size))

    # Training section
    print('Creating training model...')
    model_squeeznet = SqueezeNet()  # Squeeznet model takes more time to train but has faster evaluation than vgg16
    model = create_model(labels, img_size)

    print('Compiling model...')
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

    print('Loading training data...')
    train_data = load_training_data(labels, img_size)
    train_images = np.array([i[0] for i in train_data]).reshape(-1, img_size, img_size, 1)
    train_labels = get_labels(np.array([i[1] for i in train_data]))

    print('Fitting model...')
    model.fit(train_images, train_labels, batch_size=10, epochs=3, verbose=1)

    print('Saving model...')
    model.save_weights(OUT + 'model.h5')

    # Testing section
    print('Loading test data...')
    test_data = load_test_data(labels, img_size)

    test_images = np.array([i[0] for i in test_data]).reshape(-1, img_size, img_size, 1)
    test_labels = get_labels(np.array([i[1] for i in test_data]))
    print('Evaluating test data...')
    loss, acc = model.evaluate(test_images, test_labels, verbose=1)
    print(acc * 100)


# Want to know how we should format the height x width image data dimensions
# for inputting to a keras model
def get_size_statistics(lables):
    heights = []
    widths = []
    img_count = 0
    for label in lables:
        directory = TRAIN_DIR + '/' + label
        for img in os.listdir(directory):
            path = os.path.join(directory, img)
            if "DS_Store" not in path:
                data = np.array(Image.open(path))
                heights.append(data.shape[0])
                widths.append(data.shape[1])
                img_count += 1
    avg_height = sum(heights) / len(heights)
    avg_width = sum(widths) / len(widths)
    print("Average Height: " + str(avg_height))
    print("Max Height: " + str(max(heights)))
    print("Min Height: " + str(min(heights)))
    print('\n')
    print("Average Width: " + str(avg_width))
    print("Max Width: " + str(max(widths)))
    print("Min Width: " + str(min(widths)))
    return int(min(avg_height, avg_width))


def load_training_data(labels, img_size):
    train_data = []
    for label in labels:
        directory = TRAIN_DIR + '/' + label
        for img in os.listdir(directory):
            path = os.path.join(directory, img)
            if "DS_Store" not in path:
                img = Image.open(path)
                img = img.convert('L')
                img = img.resize((img_size, img_size), Image.ANTIALIAS)
                train_data.append([np.array(img), label])
    shuffle(train_data)
    return train_data


def load_test_data(labels, img_size):
    test_data = []
    for label in labels:
        directory = TRAIN_DIR + '/' + label
        for img in os.listdir(directory):
            path = os.path.join(directory, img)
            if "DS_Store" not in path:
                img = Image.open(path)
                img = img.convert('L')
                img = img.resize((img_size, img_size), Image.ANTIALIAS)
                test_data.append([np.array(img), label])
    shuffle(test_data)
    return test_data


def create_model(labels, img_size):
    model = Sequential()
    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', input_shape=(img_size, img_size, 1)))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(96, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(96, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(BatchNormalization())
    model.add(Dropout(0.2))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(128, activation='relu'))
    # model.add(Dropout(0.3))
    model.add(Dense(len(labels), activation='softmax'))
    return model


def get_labels(y):
    encoder = LabelEncoder()
    encoder.fit(y)
    encoded_y = encoder.transform(y)
    return to_categorical(encoded_y)


if __name__ == "__main__":
    main()
