import os
from random import randint, shuffle
import numpy as np
import cv2
import matplotlib.pyplot as plt
import time


# image processing functions


def create_image_color_histogram(image):
    """Helper function that creates an image color histogram

    Parameters:
        image: cv2 image
    """

    color = ('b', 'g', 'r')

    for i, col in enumerate(color):
        histogram = cv2.calcHist([image], [i], None, [256], [0, 256])
        plt.plot(histogram, color=col)
        plt.xlim([0, 256])

    plt.show()
    k = cv2.waitKey(0)


def create_hsv_image_mask(image, filter_lower, filter_upper):
    """Creates an image mask based on custom lower and upper hsv filter values

    Parameters:
        image: cv2 image in hsv color space
        filter_lower: lower hsv filter (eg. 0, 100, 100)
        filter_upper: upper hsv filter (eg. 50, 255, 255)

    Returns:
        image_mask: cv2 image mask
    """
    lower = np.array(filter_lower, np.uint8)
    upper = np.array(filter_upper, np.uint8)
    mask = cv2.inRange(image, lower, upper)
    image_mask = cv2.bitwise_and(image, image, mask=mask)

    return image_mask


def image_has_led(image, threshold, filter_lower, filter_upper):
    """Returns an information weather an image contains a LED turned on

    Parameters:
        image: cv2 image in hsv color space
        threshold(int): threshold value of a minimal pixel area of a detected blob that represents a LED (eg. 50)
        filter_lower: lower hsv filter of the LED color (eg. 0, 100, 100)
        filter_upper: upper hsv filter of the LED color (eg. 50, 255, 255)

    Returns:
        has_led(boolean): true if image contains a working LED, otherwise false
    """
    # blur image to get rid of contours appearing from image noise
    blur_image = cv2.medianBlur(image, 3)

    # convert image to HSV color space in order to separate luminance and chrominance parts and filter with desired mask
    hsv_image = cv2.cvtColor(blur_image, cv2.COLOR_BGR2HSV)
    masked_image = create_hsv_image_mask(hsv_image, filter_lower, filter_upper)

    # reduce noise in masked image with gaussian filter
    full_image = cv2.GaussianBlur(masked_image, (9, 9), 2, 2)

    # convert image to black and white space
    image_gray = cv2.cvtColor(full_image, cv2.COLOR_BGR2GRAY)

    # find contours in the image and check if there is one bigger than that of a threshold  area value
    contours, hierarchy = cv2.findContours(image_gray, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    has_led = 0
    for cnt in contours:
        # max_cnt = cv2.contourArea(cnt) if cv2.contourArea(cnt) > max_cnt else max_cnt
        if has_led:
            break
        if cv2.contourArea(cnt) > threshold:
            has_led = 1

    return has_led


# genetic algorithm functions


def goodness(dataset, filter_lower, filter_upper, threshold):
    """Calculates goodness(percentage of rightly predicted) of given parameters on a dataset for LEDs off,
     LEDs on and combined values.

        Parameters:
            dataset(str): routers dataset (black or white)
            threshold(int): threshold value of a minimal pixel area of a detected blob that represents a LED (eg. 50)
            filter_lower: lower hsv filter of the LED color (eg. 0, 100, 100)
            filter_upper: upper hsv filter of the LED color (eg. 50, 255, 255)

        Returns:
            (goodness_all, goodness_on, goodness_off): a tuple containing percentages of rightly predicted images
     """
    wrong_on, right_on = 0, 0
    wrong_off, right_off = 0, 0

    # calculate percentage of correct predictions for LEDs on
    image_path_black_on = "training_set\\" + dataset + "\\lampice\\upaljeno\\"
    files = os.listdir(image_path_black_on)
    shuffle(files)

    # images = [cv2.imread(image_path_black_on + file) for file in files[0:50]]
    images = [cv2.imread(image_path_black_on + file) for file in files]

    for image in images:
        res = image_has_led(image, threshold, filter_lower, filter_upper)
        if res:
            right_on += 1
        else:
            wrong_on += 1
    goodness_on = right_on / (right_on + wrong_on)
    # print("on: " + str(goodness_on))

    # calculate percentage of correct predictions for LEDs off
    image_path_black_on = "training_set\\" + dataset + "\\lampice\\ugaseno\\"
    files = os.listdir(image_path_black_on)
    shuffle(files)
    # images = [cv2.imread(image_path_black_on + file) for file in files[0:50]]
    images = [cv2.imread(image_path_black_on + file) for file in files]

    for image in images:
        res = image_has_led(image, threshold, filter_lower, filter_upper)
        if res:
            wrong_off += 1
        else:
            right_off += 1

    goodness_off = right_off / (right_off + wrong_off)
    # print("off: " + str(goodness_off))

    goodness_all = (goodness_on + goodness_off) / 2
    # print("both: " + str(goodness_all))

    return goodness_all, goodness_on, goodness_off


def crossover_pop(population_1, population_2):
    """Get a crossover population from two parent populations on averaging their values with upper and lower limits.

            Parameters:
                population_1: contains filter values  in a tuple representing a population
                population_2: contains filter values  in a tuple representing a population

            Returns:
                new_population: population generated from two parent populations
    """
    new_population = ((mean_with_limit(population_1[0][0], population_2[0][0], 0, 255),
                       mean_with_limit(population_1[0][1], population_2[0][1], 0, 255),
                       mean_with_limit(population_1[0][2], population_2[0][2], 0, 255)),
                      (mean_with_limit(population_1[1][0], population_2[1][0], 0, 255),
                       mean_with_limit(population_1[1][1], population_2[1][1], 0, 255),
                       mean_with_limit(population_1[1][2], population_2[1][2], 0, 255)),
                      mean_with_limit(population_1[2], population_2[2], 0, 150))

    return new_population


def mean_with_limit(el1, el2, low_lim, up_lim):
    el = int((el1 + el2) / 2)
    el = el if el <= up_lim else up_lim
    el = el if el >= low_lim else low_lim
    return el


def optimize_parameters_with_genetic_algorithm(dataset, popsize=20, max_iterations=1000):
    """Optimize the image_has_led function using elitistic generational genetic algorithm.

                Parameters:
                    dataset(str): black or white
                    popsize: population size
                    max_iterations: max iterations
    """

    # generate initial population using some empirically found good and some random values
    population = [((23, 75, 88), (100, 255, 255), 9)]
    for i in range(0, popsize + 1):
        population.append(((randint(0, 255), randint(0, 225), randint(0, 255)),
                           (randint(0, 255), randint(0, 225), randint(0, 255)),
                           randint(0, 100)))

    maxv = 0
    best_pop = []
    for i in range(0, max_iterations + 1):
        start = time.time()
        print("#####")
        print("iteration: " + str(i + 1))
        fitness = []

        # sort by goodness
        for i, pop in enumerate(population):
            start_pop = time.time()

            goodness_value = goodness(dataset, population[i][0], population[i][1], population[i][2])[0]
            fitness.append((goodness_value, i))

            best_pop = population[i] if goodness_value > maxv else best_pop
            maxv = goodness_value if goodness_value > maxv else maxv

            print("pop: " + str(i) + "/" + str(popsize) + " - " + str(population[i]) + " in " + str(
                time.time() - start_pop) + " seconds with goodness " + str(goodness_value))

        fitness.sort(key=lambda a: a[0], reverse=True)
        new_population = []
        for fit in fitness[:5]:
            new_population.append(population[fit[1]])

        # elitism
        population = new_population.copy()

        # population = []
        while len(population) < popsize:
            population.append(crossover_pop(population[randint(0, 4)], population[randint(0, 4)]))

        # mutation towards good heuristic
        for i in range(popsize - 4):
            if randint(0, 5) == 1:
                population[i + 4] = crossover_pop(population[i + 4],
                                                  ((randint(0, 255), randint(0, 225), randint(0, 255)),
                                                   (randint(0, 255), randint(0, 225), randint(0, 255)),
                                                   randint(0, 100)))

        print("max: " + str(maxv))
        print("best pop: " + str(best_pop))
        print("took: " + str(time.time() - start) + "seconds.")


def main():
    # some good black results
    # population = [((30, 100, 100), (50, 255, 255), 10)]
    # population = [((75, 15, 40), (138, 132, 47), 4)]
    # some good white results
    # population = [((23, 75, 88), (100, 255, 255), 9)]
    # population = [((140, 98, 61), (168, 46, 95), 9)]
    # population = [((19, 60, 151), (56, 218, 193), 25)]
    # population = [((121, 125, 108), (199, 163, 159), 23)]

    # print(population[0])
    # print(goodnes("white", population[0][0], population[0][1], population[0][2]))
    # #
    # exit()

    optimize_parameters_with_genetic_algorithm('white')


if __name__ == '__main__':
    main()
