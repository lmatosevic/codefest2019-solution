# Installation

- Make sure you are using python v3.7+. To check which version you are using run `python -v`. If you haven't installed
python yet, download it from [python official website](https://www.python.org/downloads/).

- Install packages cv2 (opencv), imutils, numpy, scipy and matplotlib, if using pip run:
`pip install opencv-python imutils numpy scipy matplotlib`.


# Project structure

- `test` folder contains all of the test images which can be obtained with `test\download-images.py` using images from 
google search

- `training` folder contains all of the training images which can be populated by unziping the `training.zip` file

- `validation` folder contains all of the validation images which can be populated by unziping the `validacijski folder.zip` file

- `detect_cable_status.py` contains cable detection functionalities alongside genetic algorithm parameters optimizer

- `detect_led_status.py` contains led detection functionalities alongside genetic algorithm parameters optimizer

- `validate-*.py` validate cable, led, model files contain scripts that generate hackathon required csv results files

- `model-borw-cnn.h5` contains trained router detection cnn model


```
│
└───test
│   │   
│   │   ...
│   download-images.py.py
│
└───training
│   │   
│   │   ...
│
└───validation
│   │   
│   │   ...
│
detect_cable_status.py
detect_led_status.py
model-borw-cnn.h5
validate-cabel.py
validate-led.py
validate-model.py
README.md
```