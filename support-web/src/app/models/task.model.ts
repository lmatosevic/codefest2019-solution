export class Task {
  id: number;
  status: string;
  name: string;
  description: string;
  sender: string;
}
