import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {RestService} from 'ngx-restful';
import {ResourceDataSource} from './resource-datasource';

@Component({
  selector: 'app-resource-table',
  templateUrl: './resource-table.component.html',
  styleUrls: ['./resource-table.component.css']
})
export class ResourceTableComponent implements OnInit, OnChanges {
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  @Input() title: string = 'Resources';
  @Input() subtitle: string = 'Listed out.';
  @Input() service: RestService<any, any>;
  @Input() extraPath: string = null;
  @Input() columns: Array<TableColumn> = [];
  @Input() actions: Array<TableAction> = [];
  @Input() buttons: Array<TableButton> = [];
  @Input() pageSize: number = 25;
  @Input() pageIndex: number = 0;
  @Input() pageOptions: Array<number> = [5, 10, 25, 50, 100];
  @Input() sortBy: string = 'created_at';
  @Input() desc: boolean = true;
  @Input() dateFormat: string = 'yyyy/MM/dd';
  @Input() timeFormat: string = 'HH:mm:ss';
  @Input() dateTimeFormat: string = 'yyyy/MM/dd HH:mm:ss';

  @Output() action = new EventEmitter<TableEvent>();
  @Output() button = new EventEmitter<TableEvent>();

  loading: boolean = true;
  total: number = 0;
  colType = ColumnType;

  public dataSource: ResourceDataSource<any>;

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.service) {
      this.service.getResponse({params: {limit: 0}}).subscribe(resp => {
        this.total = +resp.headers.get('X-Total-Count');
        this.initTable();
        this.loading = false;
      });
    }
  }

  displayColumns(): string[] {
    return this.columns.map(col => col.name);
  }

  transformElement(col: TableColumn, row): any {
    let parts = col.name.split('.');
    let elem = row[parts[0]];
    for (let i = 1; i < parts.length; i++) {
      elem = elem[parts[i]];
    }
    if (col.transform) {
      elem = col.transform(elem);
    }
    if (col.type === ColumnType.BOOLEAN) {
      return elem;
    }
    return elem ? elem : '-';
  }

  emitAction(name, row): void {
    this.action.emit({name: name, data: row});
  }

  emitButton(name): void {
    this.button.emit({name: name});
  }

  applyFilter(filterValue: any) {
    this.dataSource.filter = filterValue;
  }

  initTable(): void {
    this.sort.active = this.sortBy;
    this.sort.start = this.desc ? 'desc' : 'asc';
    this.sort.direction = this.desc ? 'desc' : 'asc';
    this.paginator.pageSize = this.pageSize;
    this.paginator.pageIndex = this.pageIndex;
    if (this.actions.length > 0 && !this.columns.find(col => col.type === ColumnType.ACTION)) {
      this.columns.push({name: 'actions', title: 'Actions', type: ColumnType.ACTION});
    }
    this.dataSource = new ResourceDataSource<any>(this.service, this.sort, this.paginator, this.extraPath);
  }
}

export interface TableColumn {
  name: string;
  title: string;
  type: ColumnType;
  options?: ColumnOptions;
  transform?: (elem) => any | null;
}

export interface ColumnOptions {
  sortable?: boolean;
  flex?: number;
  width?: string;
  alignment?: string;
}

export interface TableAction {
  name: string;
  icon: string;
  tooltip?: string;
  classes?: string;
  excludeIds?: number[];
}

export interface TableButton {
  name: string;
  text?: string;
  icon?: string;
  color?: string;
  classes?: string;
}

export interface TableEvent {
  name: string;
  data?: any;
}

export enum ColumnType {
  INTEGER,
  DECIMAL,
  STRING,
  BOOLEAN,
  DATE,
  TIME,
  DATETIME,
  ACTION
}
