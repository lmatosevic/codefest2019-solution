import {Component, OnInit} from '@angular/core';
import {TaskService} from "./services/task.service";
import {Task} from "./models/task.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string = 'support-web';

  todoList: Array<Task> = [];
  progressList: Array<Task> = [];
  doneList: Array<Task> = [];
  solved_bot = -1

  constructor(private taskService: TaskService) {
  }

  ngOnInit(): void {
    this.taskService.getAll().subscribe(tasks => {
      tasks.forEach(task => {
        if (task.status === 'TODO') {
          this.todoList.push(task);
        }
        if (task.status === 'IN_PROGRESS') {
          this.progressList.push(task);
        }
        if (task.status === 'DONE') {
          this.doneList.push(task);
        }
      })

      this.solved_bot = this.doneList.filter(el => el.sender == 'bot').length
    });
  }

}
